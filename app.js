const express = require("express"); // web server
const path = require("path"); // routes generator
const { v4 } = require("uuid"); // id's generator
const server = express(); // because Express it's server =)

let NOTES = [{ id: v4(), name: "test", value: "test", marked: false }]; // fake data

server.use(express.json()); // need for sending POST requests in json

// GET
server.get("/api/notes", (req, res) => {
  // route '/api/ourname' meaning just API request to server, even when we don't have some special route 'url/api/ourname'
  setTimeout(() => {
    // timeout just for showing loading spinner nothing more
    res.status(200).json(NOTES); // set status OK
  }, 1000);
});

//POST
server.post("/api/notes", (req, res) => {
  const note = { ...req.body, id: v4(), marked: false }; // == form.body
  NOTES.push(note);
  res.status(201).json(note); // set status created
});

//DELETE
server.delete("/api/notes/:id", (req, res) => {
  NOTES = NOTES.filter((item) => item.id !== req.params.id);
  res.status(200).json({ message: "Note has been delete" });
});

//PUT
server.put("/api/notes/:id", (req, res) => {
  const idPut = NOTES.findIndex((item) => item.id === req.params.id);
  NOTES[idPut] = req.body;
  res.status(200).json(NOTES[idPut]);
});

// all next server requests must placed in the bottom of your code
server.use(express.static(path.resolve(__dirname, "client")));
server.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "client", "index.html"));
});
server.listen(3000, () => console.log("server is working..."));
