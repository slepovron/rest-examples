import Vue from "https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.esm.browser.js";

Vue.component("loader", {
  template: `
    <div style="display: flex;justify-content: center;align-items: center">
      <div class="spinner-border" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>
  `,
});

new Vue({
  el: "#app",
  data() {
    return {
      loading: false, // form must be seen
      form: {
        name: "",
        value: "",
      },
      notes: [],
    };
  },

  computed: {
    isCreated() {
      return this.form.name && this.form.value; // if input empty button will muted
    },
  },

  methods: {
    async createNote() {
      const { ...note } = this.form; // create {note} from form
      const newNote = await request("/api/notes", "POST", note);
      this.notes.push(newNote); // add created note in [notes]
      this.form.name = this.form.value = ""; // clean input fields
    },
    async markNote(id) {
      const note = this.notes.find((item) => item.id === id);
      const updated = await request(`/api/notes/${id}`, "PUT", {
        ...note,
        marked: true,
      });
      note.marked = updated.marked;
      // return (note.marked = true);
    },
    async deleteNote(id) {
      await request(`/api/notes/${id}`, "DELETE");
      this.notes = this.notes.filter((item) => item.id !== id);
    },
  },

  async mounted() {
    // mounted() function from Vue, waited when all components will be ready
    this.loading = true; // start loader spinner
    this.notes = await request("/api/notes");
    this.loading = false;
  },
});

// REST requests template
async function request(url, method = "GET", data = null) {
  try {
    const headers = {}; // empty object for GET request
    let body;

    if (data) {
      headers["Content-Type"] = "application/json"; // standart content-type
      body = JSON.stringify(data); // {js} to json string
    }

    const response = await fetch(url, {
      method,
      headers,
      body,
    });
    return await response.json();
  } catch (e) {
    console.warn("Error: ", e.message);
  }
}
