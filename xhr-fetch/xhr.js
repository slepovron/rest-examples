const requestURL = "https://jsonplaceholder.typicode.com/users";

// const xhr = new XMLHttpRequest();

// xhr.open("GET", requestURL);

// // console.log(JSON.parse(xhr.response));
// xhr.responseType = "json";

// xhr.onload = () => {
//   if (xhr.status >= 400) {
//     console.error("error: ", xhr.response);
//   } else {
//     console.log(xhr.response);
//   }
// };

// xhr.onerror = () => {
//   console.log(xhr.response);
// };

// xhr.send();

function sendRequest(method, url, body = null) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.responseType = "json";
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = () => {
      if (xhr.status >= 400) {
        reject("error: ", xhr.response);
      } else {
        resolve(xhr.response);
      }
    };
    xhr.onerror = () => {
      reject(xhr.response);
    };
    xhr.send(JSON.stringify(body));
  });
}

console.log("sendRequest GET:");
sendRequest("GET", requestURL)
  .then((data) => console.log(data))
  .catch((err) => console.log(err));

console.log("sendRequest POST:");
body = {
  name: "Name",
  age: 32,
};
sendRequest("POST", requestURL, body)
  .then((data) => console.log(data))
  .catch((err) => console.log(err));
