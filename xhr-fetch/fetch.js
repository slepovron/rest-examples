const requestURL = "https://jsonplaceholder.typicode.com/users";

// fetch(requestURL).then((response) => {
//   console.log(response.json());
// });

function sendRequest(method, url, body = null) {
  const headers = {
    "Content-Type": "application/json",
  };
  return fetch(url, {
    method: method,
    body: JSON.stringify(body),
    headers: headers,
  }).then((response) => {
    if (response.ok <= 400) {
      return response.json();
    } else {
      throw new Error("Some error");
    }
  });
}

// console.log("sendRequest GET:");
// sendRequest("GET", requestURL)
//   .then((data) => console.log(data))
//   .catch((err) => console.log(err));

console.log("sendRequest POST:");
body = {
  name: "Name",
  age: 32,
};
sendRequest("POST", requestURL, body)
  .then((data) => console.log(data))
  .catch((err) => console.log(err));
